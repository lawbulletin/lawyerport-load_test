package tests;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.common.base.Verify;


import helperclasses.TimersAndClocks;
import helperclasses.WaitsOfAllSorts;
import pages.EditPrimaryContactInfo;
import pages.AddCaseExperiencePopup;
import pages.AddCivicAffiliationPopup;
import pages.AddCourtAndOtherAdmissionsPopup;
import pages.AddEducationPopup;
import pages.AddHonorsPopup;
import pages.AddMatterExperiencePopup;
import pages.AddNewsPopup;
import pages.AddProfessionalAffiliationPopup;
import pages.AddPublicationPopup;
import pages.AddSpeakingPopup;
import pages.AddStatePopup;
import pages.AdmissionsAndEducationPage;
import pages.AdmissionsPage;
import pages.AffiliationsAndHonorsPage;
import pages.AffiliationsPage;
import pages.BiographyPopup;
import pages.CasesAndMattersPage;
import pages.CasesPage;
import pages.PlaceOfBirthPopup;
import pages.BioLanguagesPopup;
import pages.BiographyPage;
import pages.ContactInformationPage;
import pages.DeletePopup;
import pages.EditNamePopup;
import pages.EditProfilePage;
import pages.EducationPage;
import pages.HonorsPage;
import pages.IndustriesPage;
import pages.IndustriesPopup;
import pages.LandingPage;
import pages.LoginPage;
import pages.MainPage;
import pages.MattersPage;
import pages.NewsPage;
import pages.NewsPubsAndSpeakingPage;
import pages.PracticeAreasPage;
import pages.PracticeAreasPopup;
import pages.PublicationsPage;
import pages.SpeakingPage;

public class AdminEditDirectoryTest {
	
	//final String baseURL = "https://www.lawyerportaws.com/";
	final String baseURL = "https://www.lawyerport.com/";
	//final String baseURL = "https://www.qa.lawyerport.com/";
	
	private LawyerportUsersManager users = new LawyerportUsersManager("elp");
	LawyerportUser user = users.getUserAccount();
	String password = "Testpassword1!!";
	String clientCode = "Selenium Admin Edit";
	
	
	@Test (threadPoolSize = 3, invocationCount = 20)
	public void adminEditDirectoryTest() throws MalformedURLException, InterruptedException{
		
//		WebDriver driver = new RemoteWebDriver(new URL("http://ec2-52-8-231-127.us-west-1.compute.amazonaws.com:4444/wd/hub"), DesiredCapabilities.firefox());
		WebDriver driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.firefox());
//		WebDriver driver = new RemoteWebDriver(new URL("http://68.250.141.254:4444/wd/hub"), DesiredCapabilities.firefox());
//		WebDriver driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
//		WebDriver driver = new RemoteWebDriver(new URL("http://172.16.2.29:5555/wd/hub"), DesiredCapabilities.firefox());//juratest8
		
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	LawyerportUser user = users.getUserAccount();
	LawyerportUser userLastName = users.getLastName();
	
	/*
	 * Logging In
	 */
	
	Reporter.log("Logging in", true);
	driver.get(baseURL);
	driver.findElement(By.id("signin-button")).click();
	driver.findElement(By.id("resetButton")).click();
	driver.findElement(By.id("username")).click();
	driver.findElement(By.id("username")).sendKeys(user.getUsername());
	Reporter.log("User has signed in as " +  user.getUsername(), true);
	driver.findElement(By.id("password")).click();
	driver.findElement(By.id("password")).sendKeys(password);
	driver.findElement(By.id("clientCode")).sendKeys(clientCode);
	driver.findElement(By.id("submitButton")).click();
	Reporter.log("Log in complete", true);
	
	/*
	 * Name Edit
	 */
	
	Random randomNumber = new Random();
	
	Thread.sleep(10000);
	Reporter.log("Editing Name", true);
	driver.findElement(By.id("button-welcome-link")).click();
	driver.findElement(By.id("my-profile-edit")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("button-title-edit")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("form-field-1")).click();
	driver.findElement(By.id("form-field-1")).clear();
	String currentName = ("name" + randomNumber.nextInt(20));
	driver.findElement(By.id("form-field-1")).sendKeys("Name" + randomNumber.nextInt(20));
	driver.findElement(By.id("form-button-Save")).click();
	Reporter.log("Editing Name complete", true);
	
	/*
	 * Title Edit
	 */
	Reporter.log("Editing title", true);
	Thread.sleep(10000);
	driver.findElement(By.id("button-welcome-link")).click();
	driver.findElement(By.id("my-profile-edit")).click();
	driver.findElement(By.xpath("//span[@id[starts-with(.,'button-persons-contact-edit')]]")).click(); //Clicking "Edit Primary Contact Information", dynamic id, so we use xpath
	driver.findElement(By.id("form-field-2")).click();
	driver.findElement(By.id("form-field-2")).clear();
	driver.findElement(By.id("form-field-2")).sendKeys("Title" + randomNumber.nextInt(20));
	driver.findElement(By.id("form-button-Save")).click();
	Reporter.log("Editing title complete", true);
	
	/*
	 * Bio (Modify & Delete)
	 */
	
	Thread.sleep(10000);	
	Reporter.log("Modifying and deleting bio", true);
	driver.findElement(By.id("button-welcome-link")).click();
	driver.findElement(By.id("my-profile-edit")).click();
	driver.findElement(By.id("pA-toc-biography")).click();
	driver.findElement(By.id("button-persons-bio-bio-edit")).click();
	driver.switchTo().frame(0);
	driver.findElement(By.xpath("//body")).click();
	driver.findElement(By.xpath("//body")).clear();
	driver.findElement(By.xpath("//body")).sendKeys("Test");
	Thread.sleep(2000);
	driver.switchTo().defaultContent();
	driver.findElement(By.xpath("//a[@id='form-button-Save']")).click();
	Reporter.log("Bio edit complete", true);
	
	Reporter.log("Deleting bio", true);
	Thread.sleep(4000);
	driver.findElement(By.id("button-persons-bio-bio-edit")).click();
	driver.switchTo().frame(0);
	driver.findElement(By.xpath("//body")).click();
	driver.findElement(By.xpath("//body")).clear();
	Thread.sleep(2000);
	driver.switchTo().defaultContent();
	driver.findElement(By.xpath("//a[@id='form-button-Save']")).click();
	Reporter.log("Deleting bio complete", true);
	
	Reporter.log("Modifying and deleting bio complete", true);
	
	/*
	 * Practice Areas (AOP)
	 */
	
	Thread.sleep(10000);
	Reporter.log("Editing and deleting Pratice Areas (AOP)", true);
	driver.findElement(By.id("button-welcome-link")).click();
	driver.findElement(By.id("my-profile-edit")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("pA-toc-practice-areas")).click();
	
	try {
		driver.findElement(By.xpath("//span[@id[starts-with(.,'button-persons-aop-edit')]]")).click();
	} catch (org.openqa.selenium.NoSuchElementException e) {
		System.out.println("PRESSING EDIT");
		driver.findElement(By.id("edit-profile")).click();
		driver.findElement(By.xpath("//span[@id[starts-with(.,'button-persons-aop-edit')]]")).click();
	}
	
	//driver.findElement(By.id("menu-persons-aop-add-item-0")).click();
	driver.findElement(By.id("aop-full-item-2")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath("//a[@class='aop-edit-select link-btn']")).click();
	driver.findElement(By.id("form-button-Save")).click();
	Reporter.log("AOP Edited", true);
	
	driver.findElement(By.id("button-persons-aop-edit-0")).click();
	driver.findElement(By.id("aop-clear-all")).click();
	driver.findElement(By.id("form-button-Save")).click();
	Reporter.log("AOP Deleted", true);
	Reporter.log("Editing and deleting Pratice Areas (AOP) complete", true);
	
	
	/*
	 * Work History
	 */
	
	Thread.sleep(10000);
	Reporter.log("Creating, modifying and deleting Work History", true);
	driver.findElement(By.id("button-welcome-link")).click();
	driver.findElement(By.id("my-profile-edit")).click();
	Thread.sleep(3000);
	Reporter.log("Creating work history", true);
	driver.findElement(By.id("pA-toc-work-history-li")).click();
	try {
		driver.findElement(By.id("button-persons-history-add")).click();
	} catch (org.openqa.selenium.NoSuchElementException e) {
		driver.findElement(By.id("button-persons-history-add")).click();
	}
	
	driver.findElement(By.xpath("//input[@class='yui3-tokeninput-input']")).sendKeys("Test Organization");
	driver.findElement(By.xpath("//input[@class='yui3-tokeninput-input']")).sendKeys(Keys.RETURN);
	driver.findElement(By.id("form-field-2")).sendKeys("Test Title");
	driver.findElement(By.id("form-field-7")).click();
	driver.findElement(By.xpath("//select[@id='form-field-7']/option[@value='1']")).click();
	driver.findElement(By.id("form-field-8")).click();
	driver.findElement(By.xpath("//select[@id='form-field-8']/option[@value='2017']")).click();
	driver.findElement(By.id("form-field-9")).click();
	driver.findElement(By.xpath("//select[@id='form-field-9']/option[@value='-1']")).click();
	driver.findElement(By.id("form-field-10")).click();
	driver.findElement(By.xpath("//select[@id='form-field-10']/option[@value='-1']")).click();
	driver.findElement(By.id("form-button-Save")).click();
	Reporter.log("Work history created", true);
	
	Reporter.log("Modifying work history");
	driver.findElement(By.xpath("//span[@id='button-persons-history-edit-0']")).click();
	driver.findElement(By.xpath("//li[@class='yui3-tokeninput-item yui3-tokeninput-token']")).sendKeys(Keys.DELETE);;
	Thread.sleep(2000);
	driver.findElement(By.xpath("//input[@class='yui3-tokeninput-input']")).sendKeys("Test Organization Edited");
	driver.findElement(By.xpath("//input[@class='yui3-tokeninput-input']")).sendKeys(Keys.ENTER);
	driver.findElement(By.id("form-field-2")).clear();;
	driver.findElement(By.id("form-field-2")).sendKeys("Test Title Edited");
	driver.findElement(By.id("form-field-7")).click();
	driver.findElement(By.xpath("//select[@id='form-field-7']/option[@value='2']")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("form-button-Save")).click();
	driver.findElement(By.id("form-button-Save")).click();
	Reporter.log("Work history edited", true);
	
	Reporter.log("Deleting work history", true);
	Thread.sleep(2000);
	driver.findElement(By.id("button-persons-history-del-0")).click();
	driver.findElement(By.id("form-button-Yes")).click();
	Reporter.log("Work history deleted", true);
	
	/*
	 * Education
	 */
	
	Thread.sleep(10000);
	Reporter.log("Creating, modifying & Deleting Education", true);
	driver.findElement(By.id("button-welcome-link")).click();
	driver.findElement(By.id("my-profile-edit")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("pA-toc-admissions")).click();
	driver.findElement(By.id("persons-subtab-education")).click();
	driver.findElement(By.id("button-persons-admissions-education-add")).click();
	driver.findElement(By.xpath("//input[@class='yui3-tokeninput-input']")).sendKeys("depaul");
	driver.findElement(By.xpath("//li[@data-text='DePaul University']")).click();
	driver.findElement(By.id("form-button-Save")).click();
	Reporter.log("Created education", true);
	
	Reporter.log("Modifying education", true);
	driver.findElement(By.id("button-persons-admissions-education-edit-0")).click();
	driver.findElement(By.id("form-field-4")).sendKeys("Major in Law");
	driver.findElement(By.id("form-button-Save")).click();
	Reporter.log("Modified education", true);
	
	Reporter.log("Deleting education", true);
	Thread.sleep(2000);
	driver.findElement(By.id("button-persons-admissions-education-del-0")).click();
	driver.findElement(By.id("form-button-Yes")).click();
	Reporter.log("Education Deleted", true);
	
	/*
	 * SOLR Verification
	 */
	
	//SOLR Verification is not possible at this time - PP 3/13/18
	
	
	
	
	}
}
