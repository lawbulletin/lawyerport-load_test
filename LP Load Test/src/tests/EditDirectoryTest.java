package tests;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.common.base.Verify;


import helperclasses.TimersAndClocks;
import helperclasses.WaitsOfAllSorts;
import pages.EditPrimaryContactInfo;
import pages.AddCaseExperiencePopup;
import pages.AddCivicAffiliationPopup;
import pages.AddCourtAndOtherAdmissionsPopup;
import pages.AddEducationPopup;
import pages.AddHonorsPopup;
import pages.AddMatterExperiencePopup;
import pages.AddNewsPopup;
import pages.AddProfessionalAffiliationPopup;
import pages.AddPublicationPopup;
import pages.AddSpeakingPopup;
import pages.AddStatePopup;
import pages.AdmissionsAndEducationPage;
import pages.AdmissionsPage;
import pages.AffiliationsAndHonorsPage;
import pages.AffiliationsPage;
import pages.BiographyPopup;
import pages.CasesAndMattersPage;
import pages.CasesPage;
import pages.PlaceOfBirthPopup;
import pages.BioLanguagesPopup;
import pages.BiographyPage;
import pages.ContactInformationPage;
import pages.DeletePopup;
import pages.EditNamePopup;
import pages.EditProfilePage;
import pages.EducationPage;
import pages.HonorsPage;
import pages.IndustriesPage;
import pages.IndustriesPopup;
import pages.LandingPage;
import pages.LoginPage;
import pages.MainPage;
import pages.MattersPage;
import pages.NewsPage;
import pages.NewsPubsAndSpeakingPage;
import pages.PracticeAreasPage;
import pages.PracticeAreasPopup;
import pages.PublicationsPage;
import pages.SpeakingPage;
import pages.WorkHistoryPage;
import pages.WorkHistoryPopup;

/**
 * @author rkennedy
 *
 */
/**
 * @author pthakkar
 *
 * @author mmroz
 */
public class EditDirectoryTest {
	
	//final String baseURL = "https://www.lawyerportaws.com/";
	final String baseURL = "https://www.lawyerport.com/";
	//final String baseURL = "https://www.qa.lawyerport.com/";
	

	private LawyerportUsersManager users = new LawyerportUsersManager("elp");
	
		
	//URL is passed in
	public void loadPageWithWait(WebDriverWait wait, WebDriver driver, String URL) throws InterruptedException {
		System.out.println(Thread.currentThread().getId());
		driver.get(URL); //URL passed in from method, THen waits for 3 elements to load
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("leftContent1")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-header-title")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-wrapper")));
		  
		System.out.println(System.currentTimeMillis());
		//driver.manage().timeouts().implicitlyWait(2000000, TimeUnit.SECONDS);
		//Thread.sleep(2000);
	}
	
	//No URL is passed in 
	public void loadPageWithWait(WebDriverWait wait, WebDriver driver) throws InterruptedException {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("leftContent1")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-header-title")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-wrapper")));
		  
		System.out.println(System.currentTimeMillis());
		//driver.manage().timeouts().implicitlyWait(2000000, TimeUnit.SECONDS);
		//Thread.sleep(2000);
	}

	
	
	/**
	 * 
	 * @throws Throwable
	 */
//	@Parameters({ "qaurl" })
	@Test(threadPoolSize = 3, invocationCount = 3)
	public void contactInformationTest() throws Throwable {
		

//		Start - End timer same day only. Does not account for different date.
//      if (new TimersAndClocks().setAppStartTime(9, 50, 0, 0).setAppEndTime(9, 55, 0, 0).isPassedEndTime()) {
//          System.out.println("LoadTest ended due to time");
//          return;
//      }
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("EditDirectoryTest started at: " + dateFormat.format(date)); //2016/11/16 12:08:43
		
		Reporter.log("Test started at " + LocalDateTime.now().toString());
		System.out.println(users);
		LawyerportUser user = users.getUserAccount();
		LawyerportUser userLastName = users.getLastName();

		System.out.println("Username: " + user.getUsername() + " Password: " + user.getPassword());
//		WebDriver driver = new RemoteWebDriver(new URL("http://ec2-52-8-231-127.us-west-1.compute.amazonaws.com:4444/wd/hub"), DesiredCapabilities.firefox());
		WebDriver driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.firefox());
//		WebDriver driver = new RemoteWebDriver(new URL("http://68.250.141.254:4444/wd/hub"), DesiredCapabilities.firefox());
//		WebDriver driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
//    	WebDriver driver = new RemoteWebDriver(new URL("http://172.16.2.29:5555/wd/hub"), DesiredCapabilities.firefox());//juratest8
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Reporter.log("Browser Opened");
		driver.get(baseURL);
		Reporter.log("Application Started");
		new LandingPage(driver).signIn();
		new LoginPage(driver).login(user.getUsername(), user.getPassword(), "SeleniumEdit");
		new MainPage(driver).selectUserMenuWelcomeLink().editMyProfile();
		Reporter.log("User has signed in as" +  user.getUsername());
		EditProfilePage editProfilePage = new EditProfilePage(driver);
		WaitsOfAllSorts wt = new WaitsOfAllSorts(driver);
		WebDriverWait wait = new WebDriverWait(driver, 100);// WebDriverWait added Amine 1/5/2018

		/*
		 * Edit profile Name test
		 */

		editProfilePage.selectEditNameButton();
		String fname = "Fred";
		String updatedName = "Edit";
		EditNamePopup editNamePopup = new EditNamePopup(driver);
		Assert.assertTrue(editNamePopup.isPopupName("Edit Name"));
		editNamePopup.namePrefix("Mr.");
		editNamePopup.enterFirstName(fname);
		editNamePopup.saveChanges();
		
		Reporter.log("Changes for  first name has been saved");

//		editProfilePage.selectEditNameButton();
//		Verify.verify(editNamePopup.getPageFactory().getEditFirstName().getAttribute("value").endsWith(fname));
		
//		editNamePopup.closePopupByIcon();
		editProfilePage.selectEditNameButton();
		editNamePopup.enterLastName("Thakkar");
		editNamePopup.saveChanges();
		
		// changing back to defaults
		editProfilePage.selectEditNameButton();
		editNamePopup.enterFirstName(updatedName);
		System.out.println(userLastName.getUsernameLast());
		editNamePopup.enterLastName(userLastName.getUsernameLast());
		editNamePopup.saveChanges();
		
		Reporter.log("Changes for last name has been saved");

		/*
		 * Edit contact information
		 *
		 * primary contact information
		 */
		ContactInformationPage editPrimaryInfo = new ContactInformationPage(driver);
		editPrimaryInfo.selectEditPrimaryContactInfo();
		EditPrimaryContactInfo editPrimaryContact = new EditPrimaryContactInfo(driver);
		String streetAddress = "1257 Fairwood Ct.";
		editPrimaryContact.enterStreetAddress(streetAddress);
		editPrimaryContact.saveChanges();
		Reporter.log("Contact details saved");
		driver.navigate().refresh();
		editPrimaryInfo.selectEditPrimaryContactInfo();
//		Verify.verify(
//				editPrimaryContact.getPageFactory().getStreetAddress().getAttribute("value").endsWith(streetAddress));
		editPrimaryContact.closePopupByButton();

		/*
		 * Biography
		 */

		editProfilePage.navigateTo("Biography");
		BiographyPage biography = new BiographyPage(driver);
		biography.selectEditPlaceOfBirth();
		PlaceOfBirthPopup editPlaceOFBirth = new PlaceOfBirthPopup(driver);
		editPlaceOFBirth.selectCountry();
		biography.selectEditLanguages();
		BioLanguagesPopup editLanguages = new BioLanguagesPopup(driver);
		editLanguages.editLanguages("English");
		biography.selectEditBio();
		BiographyPopup editBiography = new BiographyPopup(driver);
		editBiography.enterBiography("Hello");
		
		Reporter.log("Biography details saved");

		/*
		 * Practice Areas
		 */
		editProfilePage.navigateTo("Practice Areas");
		PracticeAreasPage practiceAreas = new PracticeAreasPage(driver);

		try {
			wt.wait(5);
			practiceAreas.getPageFactory().getAddButton().click();
			practiceAreas.selectFromAddButtonDropDownList("Unaffiliated");
		} catch (Exception e) {
			practiceAreas.selectEditButton();
		}

		PracticeAreasPopup practiceAreasPopup = new PracticeAreasPopup(driver);
		practiceAreasPopup.clearAllPracticeAreas();
		practiceAreasPopup.selectPracticeAreaGroup("Civil Rights (8)");
		practiceAreasPopup.selectAllPracticeAreas();
		practiceAreasPopup.saveChanges();
		
		Reporter.log("Practice Area saved");

		/*
		 * Industries
		 */
		editProfilePage.navigateTo("Industries");

		IndustriesPage industries = new IndustriesPage(driver);

		try {
			industries.selectAddButton();
			industries.selectFromAddButtonList("Unaffiliated");
		} catch (Exception e) {
			industries.selectEditButton();
		}

		IndustriesPopup industriesPopup = new IndustriesPopup(driver);
		industriesPopup.clearAllSelection();
		industriesPopup.selectIndustries("Accounting");
		industriesPopup.selectIndustries("Aerospace");
		industriesPopup.selectIndustries("Chemicals");
		industriesPopup.saveIndustriesChanges();
		Reporter.log("Industries changes saved");
		
		/*
		 * Cases & Matters :- Cases
		 */
		editProfilePage.navigateTo("Cases & Matters");
		CasesAndMattersPage navigationToTabs = new CasesAndMattersPage(driver);
		CasesPage casePage = new CasesPage(driver);
		casePage.addCaseExperience();
		AddCaseExperiencePopup caseExperiencePopup = new AddCaseExperiencePopup(driver);
		caseExperiencePopup.enterCaseCaption("A vs B");

		// Hard-code to blindly select January. Needs to be recoded.
		caseExperiencePopup.getPageFactory().getMonthDropDownButton().sendKeys(Keys.DOWN);
		caseExperiencePopup.getPageFactory().getMonthDropDownButton().sendKeys(Keys.ENTER);
		

		// Hard-code to blindly select 2014. Needs to be recoded.
		caseExperiencePopup.getPageFactory().getYearDropdownButton().sendKeys(Keys.DOWN);
		caseExperiencePopup.getPageFactory().getYearDropdownButton().sendKeys(Keys.ENTER);
		
		// //caseExperiencePopup.selectMonth("January");
		// //caseExperiencePopup.selectYear("2010");
		caseExperiencePopup.saveCase();
		Reporter.log("Case added");
		
		 casePage.caseDelete();
		 casePage.yesButton();
		 Reporter.log("Case deleted");
		 
		 
		 Reporter.log("Cases test complete");
		

		/*
		 * Cases & Matters :- Matters
		 */
		navigationToTabs.navigateToMattersTab();
		MattersPage mattersPage = new MattersPage(driver);
		// Add Matter
		mattersPage.addMatterExperience();
		AddMatterExperiencePopup addMattersExperiencePopup = new AddMatterExperiencePopup(driver);
		addMattersExperiencePopup.enterTitle("Menards");
		addMattersExperiencePopup.selectMonth("April");
		addMattersExperiencePopup.selectYear("2013");
		addMattersExperiencePopup.save();
		Reporter.log("Matter added ");
		
		// Edit Matter
		mattersPage.editMattersButton();
		addMattersExperiencePopup.enterTitle("Home Depot");
		addMattersExperiencePopup.selectMonth("February");
		addMattersExperiencePopup.selectYear("1999");
		addMattersExperiencePopup.save();
		Reporter.log("Matter Edited");
		
		// Delete Matter
		mattersPage.deleteMattersButton();
		DeletePopup deleteMattersPopup = new DeletePopup(driver);
		deleteMattersPopup.yesButton();
		Reporter.log("Matter deleted");
		
		Reporter.log("Matters test complete");

		/*
		 * News, Pubs & Speaking
		 */
		editProfilePage.navigateTo("News, Pubs & Speaking");

		// Add News Tab
		NewsPage newsPage = new NewsPage(driver);
		newsPage.addArticleButton();
		AddNewsPopup addNewsPopup = new AddNewsPopup(driver);
		addNewsPopup.titleTextbox("ABC");
		addNewsPopup.publishYear("1998");
		addNewsPopup.save();
		Reporter.log("News added");

		// Edit News Tab
		newsPage.editArticleButton();
		addNewsPopup.titleTextbox("ABCDEFGHI");
		addNewsPopup.publishYear("1979");
		addNewsPopup.save();
		Reporter.log("News edited");

		// Delete News Tab
		newsPage.deleteArticleButton();
		DeletePopup deleteNewsPopup = new DeletePopup(driver);
		deleteNewsPopup.yesButton();
		
		Reporter.log("Deleted the news");

		// // Click Publications Tab
		NewsPubsAndSpeakingPage newsPubsAndSpeakingPage = new NewsPubsAndSpeakingPage(driver);
		newsPubsAndSpeakingPage.publicationsTab();
		// Add Publication
		AddPublicationPopup addPublicationPopup = new AddPublicationPopup(driver);
		PublicationsPage publicationsPage = new PublicationsPage(driver);
		publicationsPage.addPublicationButton();
		addPublicationPopup.titleTextbox("Lorem Ipsum");
		addPublicationPopup.publishYear("1990");
		addPublicationPopup.save();
		 Reporter.log("Publication added");
		// Edit Publication
		publicationsPage.editPublicationButton();
		addPublicationPopup.titleTextbox("Lorem Ipsum Lorem Ipsum");
		addPublicationPopup.publishYear("2001");
		addPublicationPopup.save();
		 Reporter.log("Publication edited");
		// Delete Publications
		publicationsPage.deletePublicationsButton();
		DeletePopup deletePublicationsPopup = new DeletePopup(driver);
		deletePublicationsPopup.yesButton();
		 Reporter.log("Publication deleted");

		// Add Speaking
		newsPubsAndSpeakingPage.speakingTab();
		SpeakingPage speakingPage = new SpeakingPage(driver);
		speakingPage.addSpeakingButton();
		AddSpeakingPopup addSpeakingPopup = new AddSpeakingPopup(driver);
		addSpeakingPopup.speakersTextbox("Lorem Ipsum");
		addSpeakingPopup.speachTitle("It is Sunny Outside");
		addSpeakingPopup.eventTitle("My Title");
		addSpeakingPopup.eventYear("1987");
		addSpeakingPopup.save();
		 Reporter.log("Speaking added");
		// Edit Speaking
		speakingPage.editSpeakingButton();
		addSpeakingPopup.speakersTextbox("Jordan Speaker");
		addSpeakingPopup.speachTitle(" and 90 degrees");
		addSpeakingPopup.eventTitle(" New Title");
		addSpeakingPopup.save();
		 Reporter.log("Speaking edited");
		// Delete Speaking
		speakingPage.deleteSpeakingButton();
		DeletePopup deleteSpeakingPopup = new DeletePopup(driver);
		deleteSpeakingPopup.yesButton();
		 Reporter.log("Speaking deleted");
		
		
		/*
		 * Affiliations & Honors
		 */
		editProfilePage.navigateTo("Affiliations & Honors");
		
		
		/*
		 * Affiliations tab 
		 */
		AffiliationsAndHonorsPage affiliationsAndHonors = new AffiliationsAndHonorsPage(driver);
		AffiliationsPage editAffiliations = new AffiliationsPage(driver);
		 editAffiliations.addProfessionalAffiliation();
		 AddProfessionalAffiliationPopup professionalAffiliation = new AddProfessionalAffiliationPopup(driver);
		 professionalAffiliation.enterOrganization("AA");
		 professionalAffiliation.saveChanges();
		 Reporter.log("Professional organization added");
		 
		 editAffiliations.editProfessionalAffiliation();
		 professionalAffiliation.selectFromTextbox();
		 professionalAffiliation.enterOrganization("bb");
		 professionalAffiliation.saveChanges();
		 Reporter.log("Professional organization edited");
		 
		 editAffiliations.deleteProfessionalAffiliaition();
		 deleteSpeakingPopup.yesButton();
		 Reporter.log("Professional organization deleted");
		 
		 
		 editAffiliations.addCivicAffiliation();
		 AddCivicAffiliationPopup civicAffiliation = new AddCivicAffiliationPopup(driver);
		 civicAffiliation.enterOrganization("aa");
		 civicAffiliation.saveChanges();
		 Reporter.log("Civic organization added");
		 
		 editAffiliations.editCivicAffiliation();
		 professionalAffiliation.selectFromTextbox();
		 civicAffiliation.enterOrganization("bb");
		 civicAffiliation.saveChanges();
		 Reporter.log("Civic organization edited");
		 
		 editAffiliations.deleteCivicAffiliation();
		 deleteSpeakingPopup.yesButton();
		 Reporter.log("Civic Organization deleted");
		 
		 /*
		  * honors tab 
		  */
		 affiliationsAndHonors.navigateToHonors();
		 HonorsPage editHonors = new HonorsPage(driver);
		 editHonors.addHonors();
		 AddHonorsPopup honors = new AddHonorsPopup(driver);
		 honors.enterHonor("Judge");
		 honors.selectStartYear("1992");
		 honors.selectEndYear("2000");
		 honors.saveChanges();
		 Reporter.log("Honor Added ");
		 editHonors.editHonors();
		 honors.enterHonor("Lawyer");
		 honors.saveChanges();
		 Reporter.log("Honor edited");
		 
		 editHonors.deleteHonors();
		 deleteSpeakingPopup.yesButton();
		 Reporter.log("Honor deleted");
		/*
		 * Work History
		 */
		editProfilePage.navigateTo("Work History");
		WorkHistoryPage workHistoryPage = new WorkHistoryPage(driver);
		// Add Work History
		workHistoryPage.addWorkHistoryButton();
		WorkHistoryPopup workHistoryPopup = new WorkHistoryPopup(driver);
		workHistoryPopup.organizationTextbox("chi");
		workHistoryPopup.titleTextbox("Menards");
		workHistoryPopup.startMonth("April");
		workHistoryPopup.startYear("1982");
		workHistoryPopup.endMonth("February");
		workHistoryPopup.endYear("2003");
		workHistoryPopup.save();
		Reporter.log("Work History added");
		
		
		workHistoryPage.editWorkHistoryButton();
		// Edit Work History
		workHistoryPopup.startMonth("November");
		workHistoryPopup.startYear("1965");
		workHistoryPopup.endMonth("February");
		workHistoryPopup.endYear("1988");
		workHistoryPopup.save();
		Reporter.log("Work History edited");
		
		// Delete Work History
		workHistoryPage.deleteWorkHistoryButton();
		DeletePopup workHistoryDeletePopup = new DeletePopup(driver);
		workHistoryDeletePopup.yesButton();
		Reporter.log("Work History deleted");

		/*
		 * Admissions & Education
		 */
		editProfilePage.navigateTo("Admissions & Education");
		AdmissionsPage aeAdmissionsAndEducation = new AdmissionsPage(driver);

		// Add other States - with edit/delete options
		aeAdmissionsAndEducation.selectAddStateButton();
		AddStatePopup aeAdmissionsAddStatePopup = new AddStatePopup(driver);

		aeAdmissionsAddStatePopup.stateDropdown("Wisconsin");
		aeAdmissionsAddStatePopup.yearDropdown("1982");
		aeAdmissionsAddStatePopup.save();
		Reporter.log("Admission State added");

		// Edit
		aeAdmissionsAddStatePopup.editButton();
		aeAdmissionsAddStatePopup.stateDropdown("Iowa");
		aeAdmissionsAddStatePopup.yearDropdown("1964");
		aeAdmissionsAddStatePopup.save();
		Reporter.log("Admission state edited");

		// Delete Wisconsin
		aeAdmissionsAddStatePopup.deleteButton();
		DeletePopup deletePopup = new DeletePopup(driver);
		deletePopup.yesButton();
		Reporter.log("Admission State deleted");

		// Add Court & Other Admissions
		AddCourtAndOtherAdmissionsPopup aeCourts = new AddCourtAndOtherAdmissionsPopup(driver);
		aeAdmissionsAndEducation.selectAddCourtAndOtherAdmissionsButton();

		aeCourts.courtAdmissionTextbox("ha");
		aeCourts.yearDropdown("1994");
		aeCourts.save();
		Reporter.log("Court Admission added");
		
		aeAdmissionsAndEducation.otherCourtAdmissionsEditButton();
		aeCourts.selectedCourtinTextbox();
		aeCourts.courtAdmissionTextbox("ala");
		aeCourts.yearDropdown("1988");
		aeCourts.save();
		Reporter.log("Court admission edited");
		
		aeAdmissionsAndEducation.otherCourtAdmissionsDeleteButton();
		DeletePopup courtDeletePopup = new DeletePopup(driver);
		courtDeletePopup.yesButton();
		Reporter.log("Court admission deleted");

		// Select Education Tab
		AdmissionsAndEducationPage aePage = new AdmissionsAndEducationPage(driver);
		aePage.selectEducationTab();

		EducationPage aeEducationTab = new EducationPage(driver);
		aeEducationTab.addEducationButton();

		AddEducationPopup aeEducationAddPopup = new AddEducationPopup(driver);
		aeEducationAddPopup.schoolNameTextbox("Har");
		aeEducationAddPopup.gradYearDropdown("1982");
		aeEducationAddPopup.save();
		Reporter.log("Education Added");
		
		// Edit Education
		aeEducationTab.editEducationButton();
		aeEducationAddPopup.selectedSchoolinTextbox();
		aeEducationAddPopup.schoolNameTextbox("Ab");
		aeEducationAddPopup.gradYearDropdown("1970");
		aeEducationAddPopup.save();
		Reporter.log("Education edited");
		
		// Delete Education
		aeEducationTab.deleteEducationButton();
		DeletePopup educationDeletePopup = new DeletePopup(driver);
		educationDeletePopup.yesButton();
		Reporter.log("Education Deleted");

		new MainPage(driver).selectUserMenuWelcomeLink().logout();
		killDriver(driver);
		Reporter.log("Application closed");
		}
	
	

	/**
	 * @param driver
	 */
	private void killDriver(WebDriver driver) {
		driver.manage().deleteAllCookies();
		driver.close();
		driver.quit();
	}

}
