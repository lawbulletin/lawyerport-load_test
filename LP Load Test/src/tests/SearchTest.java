package tests;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.LandingPage;
import pages.LoginPage;
import pages.MainPage;

public class SearchTest {

	//final String baseURL = "http://www.lawyerportaws.com/";
	final String baseURL = "https://www.lawyerport.com/";
	//final String baseURL = "https://www.qa.lawyerport.com/";
	WebDriver driver;
	public LawyerportUsersManager users = new LawyerportUsersManager();


	@BeforeClass
	public void startBrowserAndDriver() throws MalformedURLException
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("SearchTest started at: " + dateFormat.format(date)); //2016/11/16 12:08:43
		
	//Choose Browser & Driver
		//System.setProperty("webdriver.chrome.driver", "C:/drivers/chromedriver.exe");	//Chrome
		//driver=new ChromeDriver();													//Chrome
		//System.setProperty("webdriver.gecko.driver", "C:/drivers/geckodriver.exe");	//FireFox
		//driver = new FirefoxDriver();													//FireFox
		driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.firefox()); //Grid-FireFox
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Reporter.log(":================Browser Started================");

	}
	
	
	

	@Test(threadPoolSize = 3, invocationCount = 3)
	public void startupWebDriver() throws InterruptedException 
	{
		LawyerportUser user = users.getUserAccount();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("signin-button")));
        driver.findElement(By.id("signin-button")).click();
        driver.findElement(By.id("username")).sendKeys(user.getUsername());
        driver.findElement(By.id("password")).sendKeys("Testpassword1!!");
        driver.findElement(By.id("clientCode")).sendKeys("Selenium");
        driver.findElement(By.id("submitButton")).click();


		//APS SearchTest01 (1/50)
		System.out.println("Running Appellate Case Summaries Search");
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("navRes")));
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("app-summaries")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-4")).sendKeys("closed door");
		driver.findElement(By.id("app-summaries-run-advanced-search")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search-header-title")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("AS-result-button")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search-wrapper")));
		  Thread.sleep(3000);

	//APS ViewTest01 (2/50)
		System.out.println("Clicking ACS search result");
		driver.findElement(By.id("article-link-100050153")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("article-view-text")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("article-description")));	
		  Thread.sleep(3000);
		
	//Court Calls SearchTest01 (3/50)		
		System.out.println("Running Court Call Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("court-call")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-2")).sendKeys("Bob");
		driver.findElement(By.id("court-calls-run-advanced-search")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("subTabContent2")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("court-calls-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		  Thread.sleep(3000);
			
	//Court Calls ViewTest01 (4/50)				
		System.out.println("Clicking first search result");
		driver.findElement(By.id("all")).click();
		driver.findElement(By.id("view-selected")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("subTabContent2")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("detailbottombackground")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("caseDetails")));	
		Thread.sleep(3000);			
		
	//Court Docket SearchTest01 (5/50)				
		System.out.println("Running Court Docket Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("court-dockets")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-2")).sendKeys("Grayson");
		driver.findElement(By.id("court-dockets-run-advanced-search")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("court-dockets-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		Thread.sleep(3000);				

	//Court Docket SearchTest02 (6/50)				
		System.out.println("Running Court Docket Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("court-dockets")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("court-dockets-reset-advanced-search-bottom")).click();	
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-2")).sendKeys("Jason Todd");
		driver.findElement(By.id("court-dockets-run-advanced-search")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("court-dockets-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		Thread.sleep(3000);				
			
	//Court Docket SearchTest03 (7/50)				
		System.out.println("Running Court Docket Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("court-dockets")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("court-dockets-reset-advanced-search-bottom")).click();	
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-5")).sendKeys("ROUSSELL MARY");
		driver.findElement(By.id("court-dockets-run-advanced-search")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("court-dockets-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		Thread.sleep(3000);				
		
		
	//Court Docket ViewTest01 (8/50)			
		System.out.println("Clicking first search result");
		driver.findElement(By.linkText("2014CH-3454")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("datatable-casename")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("court-dockets-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cd-back-form")));		
		Thread.sleep(3000);			
			
	//Court Rules SearchTest01 (9/50)		
		System.out.println("Running Court Rules Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("court-rules")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("civil");
		driver.findElement(By.id("court-rules-run-advanced-search")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("leftContent3")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("court-rules-centerContainer")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search-wrapper")));
		  Thread.sleep(3000);

	//Court Rules ViewTest01 (10/50)	
		System.out.println("Clicking first search result");
		driver.findElement(By.className("search-click-through")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("treeContentHolder")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("court-rules-rule-name")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("cr-content")));
		  Thread.sleep(3000);
		
	
	//Directory SearchTest01 (11/50)		
		System.out.println("Running People Search");
		driver.findElement(By.id("navDir")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("form-field-0")).sendKeys("Bob");
		driver.findElement(By.id("form-field-4")).sendKeys("Clifford");
		driver.findElement(By.id("dir-run-advanced-search")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("leftContent1")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search-header-title")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search-wrapper")));
		  Thread.sleep(3000);

	//Directory ViewTest01 (12/50)			
		System.out.println("Clicking Bob Clifford's Profile Link");
		driver.findElement(By.className("search-click-through")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("profile-pic")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("summary-profile-container")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("summary-profile-info")));
		  Thread.sleep(3000);

	//Directory SearchTest02 (13/25)		
		System.out.println("Running People Search");
		driver.findElement(By.id("navDir")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("subtab-org")).click();
		driver.findElement(By.id("form-field-0")).sendKeys("IL Supreme Court");
		driver.findElement(By.id("dir-org-run-advanced-search")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("leftContent1")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search-header-title")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search-wrapper")));
		  Thread.sleep(3000);

	//Directory ViewTest02 (14/25)			
		System.out.println("Clicking ABA's Profile Link");
		driver.findElement(By.className("result-description")).click();
		Thread.sleep(3000);


	//Directory ViewTest03 (15/25)			
		System.out.println("Navigating to Org Profile");	
		driver.get(baseURL + "/organizations#/1203271/corp-contact");
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("profile-pic")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("summary-profile-container")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("summary-profile-info")));
		  Thread.sleep(3000);
		
	
	//Directory EventTest01 (16/25)		
		System.out.println("Running People Search");
		driver.findElement(By.id("navEvents")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("left-container")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("center-container")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("event-result-title")));
		Thread.sleep(3000);	
	
	
	//Judge Profile ViewTest03 (17/25)			
		System.out.println("Navigating to Judges Profile");	
		driver.get(baseURL + "/judges#/1054016/jud-experience");
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("profile-pic")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("summary-profile-container")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("summary-profile-info")));
		Thread.sleep(3000);		
	
	
	
	//Marketplace SearchTest01 (18/25)		
		System.out.println("Running Marketplace Search");
		driver.findElement(By.id("navVend")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("law");
		driver.findElement(By.id("marketplace-search-run-advanced-search")).click();
		Thread.sleep(3000);
	
	//Marketplace ViewTest01 (19/25)	
		System.out.println("Clicking first search result");
		driver.findElement(By.className("search-click-through")).click();		
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("profile-pic")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("summary-profile-container")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("summary-profile-info")));
		  Thread.sleep(3000);
	
	//News SearchTest01 (20/50)	
		System.out.println("Running News Search");
		driver.findElement(By.id("navNews")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-4")).sendKeys("Law Bulletin Media");
		driver.findElement(By.id("news-run-advanced-search")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("left-container")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("news-center-container")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("search-detail-container")));
		  Thread.sleep(3000);

	//News ViewTest01 (21/50)
		System.out.println("Clicking first search result");
		driver.findElement(By.className("search-click-through")).click();
		Thread.sleep(3000);
				
	//SLJ SearchTest01 (22/50)	
		System.out.println("Running SLJ Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("slj")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-button-Agree")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("slj-advanced-search-form-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("num1")));
		  Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("vehicle");
		driver.findElement(By.id("slj-run-advanced-search")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("filter-0-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  Thread.sleep(3000);
		
		
	//SLJ ViewTest01 (23/50)		
		System.out.println("Clicking first search result");
		driver.findElement(By.id("all")).click();
		driver.findElement(By.id("view-selected")).click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("slj-detail-case-0")));
		Thread.sleep(3000);	

	
		//JVR SearchTest01 (24/50)		
		System.out.println("Running Verdict & Settlement Search");
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("navRes")));
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("verdicts-settlements")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("bus AND car");
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000); //only for debugging
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("form-button-submit")));
		driver.findElement(By.id("form-button-submit")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		  Thread.sleep(3000);

	//JVR ViewTest01 (25/50)	
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("view-selected")).click();
		Thread.sleep(3000);
		driver.findElement(By.tagName("body")).sendKeys("Keys.ESCAPE");
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);			

	//JVR SearchTest02 (26/50)		
		System.out.println("Running Verdict & Settlement Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("subtab-verdicts-settlements")).click();
		Thread.sleep(3000); //debugging timer
		driver.findElement(By.id("jv-reset-advanced-search-bottom")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("(burglary AND home) NOT (house OR residence)");
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		  Thread.sleep(3000);

	//JVR ViewTest02 (27/50)	
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("view-selected")).click();
		Thread.sleep(3000);	 
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);	

	//JVR SearchTest03 (28/50)		
		System.out.println("Running Verdict & Settlement Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("verdicts-settlements")).click();
		Thread.sleep(7000); //debugging timer
		driver.findElement(By.id("jv-reset-advanced-search-bottom")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("\"McDermott, Will & Emery\"");
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		  Thread.sleep(3000);

	//JVR ViewTest03 (29/50)	
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("view-selected")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);	
		
		
	//JVR SearchTest04 (30/50)		
		System.out.println("Running Verdict & Settlement Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("verdicts-settlements")).click();
		Thread.sleep(3000); //debugging timer
		driver.findElement(By.id("jv-reset-advanced-search-bottom")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("bus AND \"stop sign\"");
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		  Thread.sleep(3000);

	//JVR ViewTest04 (31/50)	
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("view-selected")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);	
	
	//JVR SearchTest05 (32/50)		
		System.out.println("Running Verdict & Settlement Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("verdicts-settlements")).click();
		Thread.sleep(3000); //debugging timer
		driver.findElement(By.id("jv-reset-advanced-search-bottom")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("Marc Soriano");
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		  Thread.sleep(3000);

	//JVR ViewTest05 (33/50)	
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("view-selected")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);		
		
	//JVR SearchTest06 (34/50)		
		System.out.println("Running Verdict & Settlement Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("verdicts-settlements")).click();
		Thread.sleep(3000); //debugging timer
		driver.findElement(By.id("jv-reset-advanced-search-bottom")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("Gary Schaer");
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		  Thread.sleep(3000);

	//JVR ViewTest06 (35/50)	
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("view-selected")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);			
		
		
	//JVR SearchTest07 (36/50)		
		System.out.println("Running Verdict & Settlement Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("verdicts-settlements")).click();
		Thread.sleep(3000); //debugging timer
		driver.findElement(By.id("jv-reset-advanced-search-bottom")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-1")).sendKeys("Gary Schaer");
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		  Thread.sleep(3000);

	//JVR ViewTest07 (37/50)	
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("view-selected")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);			
		
		
	//JVR SearchTest08 (38/50)		
		System.out.println("Running Verdict & Settlement Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("verdicts-settlements")).click();
		Thread.sleep(3000); //debugging timer
		driver.findElement(By.id("jv-reset-advanced-search-bottom")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-1")).sendKeys("Marc Soriano");
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		  Thread.sleep(3000);

	//JVR ViewTest08 (39/50)	
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("view-selected")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);			
		
	//JVR SearchTest09 (40/50)		
		System.out.println("Running Verdict & Settlement Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("verdicts-settlements")).click();
		Thread.sleep(3000); //debugging timer
		driver.findElement(By.id("jv-reset-advanced-search-bottom")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-2")).sendKeys("79L-3014");
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		Thread.sleep(3000);

	//JVR ViewTest09 (41/50)	
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("view-selected")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);		
		
		
	//JVR SearchTest10 (42/50)		
		System.out.println("Running Verdict & Settlement Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);	
		driver.findElement(By.id("verdicts-settlements")).click();
		Thread.sleep(3000);	 //debugging timer
		driver.findElement(By.id("jv-reset-advanced-search-bottom")).click();
		Thread.sleep(3000);	
		driver.findElement(By.id("form-field-1")).sendKeys("diamond");
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000);	
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		  Thread.sleep(3000);	

	//JVR ViewTest10 (43/50)	
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);	
		driver.findElement(By.id("view-selected")).click();
		Thread.sleep(3000);	
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);	
	
	//JVR SearchTest11 (44/50)		
		System.out.println("Running Verdict & Settlement Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);	
		driver.findElement(By.id("verdicts-settlements")).click();
		Thread.sleep(3000);	//debugging timer
		driver.findElement(By.id("jv-reset-advanced-search-bottom")).click();
		Thread.sleep(3000);	
		driver.findElement(By.id("form-field-0")).sendKeys("taxi");
		driver.findElement(By.id("form-field-1")).sendKeys("john");
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000);	
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-node")));
		  Thread.sleep(3000);	

	//JVR ViewTest11 (45/50)	
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);	
		driver.findElement(By.id("view-selected")).click();
		Thread.sleep(3000);	
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);	
		
				
	//JVR ViewTest12 (46/50)
		System.out.println("Navigating Directly to JVR Case");
		driver.get(baseURL + "/research#/verdicts-settlements/view-results/&fromNews=yes&juryVerdictId=1016402/1016402");	
		Thread.sleep(3000);	
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);			
	
		driver.get(baseURL);	
		Thread.sleep(3000);			
		
		//JVR ViewTest13 (47/50)
		System.out.println("Navigating Directly to JVR Case");
		driver.get(baseURL + "/research#/verdicts-settlements/view-results/&keyword=taxi&isSiteAdmin=true&publicationType=Verdict/&id=1016402&id=1015514&id=1015476&id=1015425&id=1015240&id=1014900&id=1014801&id=1014782&id=1014665&id=1014314&id=1014281&id=1014246&id=1014113&id=1014028&id=1013832&id=1013575&id=1013421&id=1013249&id=1013010&id=1012938&id=1012894&id=1012905&id=1012760&id=1012638&id=1012514&id=1012518&id=1012350&id=1012318&id=1012272&id=1012155&id=1011980&id=1011528&id=1010711&id=1009629&id=1009502&id=1009255&id=1009155&id=1007942&id=1007875&id=1007770&id=1007771&id=1007500&id=1007073&id=1006711&id=1006405&id=1006360&id=1006363&id=1006339&id=1006316&id=1005724&id=1005689&id=1005307&id=1004830&id=1004650&id=1004454&id=1004331&id=1003893&id=1003797&id=1003449&id=1002258&id=1001904&id=1001610&id=1001477&id=1001462&id=1001423&id=1001380&id=257931&id=256831&id=256262&id=253628&id=253378&id=252978&id=252532&id=251946&id=247444&id=246981&id=246384&id=245826&id=244729&id=941606&id=943434&id=945086&id=945830&id=949275&id=949666&id=950265&id=952143&id=952144&id=953929&id=955971&id=956810&id=956921&id=957853&id=959049&id=964247&id=965354&id=970283&id=971625&id=972699&id=974379&id=976709&id=977435&id=977457&id=977692&id=978685&id=979355&id=979879&id=980151&id=980654&id=980744&id=984871&id=986858&id=989804&id=994181&id=996186&id=998003&id=997437&id=997280&id=997286&id=997160&id=997167&id=996790&id=992893&id=992578&id=992636&id=992555&id=992338&id=991844&id=991599&id=989293&id=988815&id=988618&id=988639&id=986070&id=986080");	
		Thread.sleep(3000);	 
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);				

		driver.get(baseURL);	
		Thread.sleep(3000);	
		
		//JVR ViewTest14 (48/50)
		System.out.println("Navigating Directly to JVR Case");
		driver.get(baseURL + "/research#/verdicts-settlements/view-results/&keyword=bus&startDate=01%2F01%2F2017&endDate=03%2F01%2F2017&isSiteAdmin=true/&id=1014982&id=1014953&id=1014980&id=1014827&id=1014864&id=1014917&id=1014890&id=1014900&id=1014873&id=1014840");	
		Thread.sleep(3000);	 
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);	
		
		driver.get(baseURL);	
		Thread.sleep(3000);	
		
		//JVR ViewTest15 (49/50)
		System.out.println("Navigating Directly to JVR Case");
		driver.get(baseURL + "/research#/verdicts-settlements/view-results/&keyword=smith&isSiteAdmin=true&tag=9104:awtm10000/&id=1016359&id=1016168&id=1015615&id=1015665&id=1015274&id=1014869&id=1014867&id=1014733&id=1014785&id=1014613&id=1014373&id=1014270&id=1013823&id=1013874&id=1013811&id=1013681&id=1013335&id=1013365&id=1013366&id=1013156&id=1012955&id=1012353&id=1012212&id=1012211&id=1012137&id=1011950&id=1011545&id=1011289&id=1011309&id=1010983&id=1010776&id=1010600&id=1010321&id=1010324&id=1010190&id=1009843&id=1009824&id=1009687&id=1009501&id=1009504&id=1009370&id=1009356&id=1008922&id=1008492&id=1008408&id=1008063&id=1007788&id=1007158&id=1007103&id=1006770&id=1006320&id=1005914&id=1005898&id=1005650&id=1005417&id=1004934&id=1004543&id=1004251&id=1003130&id=1003367&id=1002677&id=1002632&id=1002138&id=1001613&id=1001920&id=1000882&id=258964&id=258700&id=257418&id=257666&id=256899&id=257171&id=257180&id=255455&id=255332&id=254811&id=254325&id=252482&id=253236&id=253268&id=253291&id=253290&id=253297&id=252463&id=251971&id=252027&id=251611&id=251702&id=251217&id=251182&id=248744&id=248628&id=248004&id=248051&id=247567&id=246914&id=246670&id=941419&id=941748&id=941818&id=942145&id=942890&id=943440&id=944032&id=944338&id=944510&id=945110&id=945971&id=946148&id=946309&id=946659&id=948030&id=949002&id=950055&id=951559&id=951593&id=951575&id=951621&id=952801&id=953560&id=954796&id=953627&id=955363&id=955676&id=956358&id=957261&id=957260&id=956317&id=957622&id=957922&id=958034&id=958643&id=963007&id=962368&id=963978&id=965353&id=964641&id=965883&id=967791&id=968815&id=972874&id=972351&id=973323&id=973915&id=974073&id=975456&id=978709&id=979402&id=980733&id=982084&id=982056&id=983439&id=984301&id=984683&id=985288&id=995127&id=996708&id=989349&id=992509&id=987715");	
		Thread.sleep(3000);	 
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);	
		
		driver.get(baseURL);	
		Thread.sleep(3000);	
		
		//JVR ViewTest16 (50/50)
		System.out.println("Navigating Directly to JVR Case");
		driver.get(baseURL + "/research#/verdicts-settlements/view-results/&keyword=Evans%20OR%20Diamond%20&isSiteAdmin=true&tag=1447:CXCivilRights/&id=1014191&id=1011476&id=1011313&id=1010475&id=1005330&id=1004291&id=1003888&id=1000978&id=258323&id=257540&id=256126&id=256263&id=256039&id=255056&id=254198&id=250230&id=250106&id=246487&id=245580&id=244999&id=943240&id=944096&id=944165&id=945115&id=946926&id=947005&id=947853&id=950270&id=950809&id=953226&id=953446&id=956848&id=957688&id=958985&id=962361&id=962702&id=964174&id=964555&id=965370&id=965548&id=972941&id=973103&id=977985&id=978006&id=980918&id=981020&id=990401&id=994711&id=996890&id=992737&id=992319&id=992220&id=988872&id=987086");
		Thread.sleep(3000);	 
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);	
		
		driver.get(baseURL);	
		Thread.sleep(3000);	 
		
		driver.findElement(By.id("button-welcome-link")).click();
		driver.findElement(By.id("my-profile-logout")).click();	
		
		
		
		
	}
	
	
	@AfterClass
	public void shutdown()
	{
		driver.manage().deleteAllCookies();
		driver.quit();
		
		
	}
	
	
	
}