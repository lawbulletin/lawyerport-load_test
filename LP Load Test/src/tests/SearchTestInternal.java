package tests;

import static org.testng.Assert.assertEquals;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.LandingPage;
import pages.LoginPage;
import pages.MainPage;

public class SearchTestInternal {

	//final String baseURL = "http://www.lawyerportaws.com/";
	final String baseURL = "https://www.lawyerport.com/";
	//final String baseURL = "https://www.qa.lawyerport.com/";
	WebDriver driver;
	public LawyerportUsersManager users = new LawyerportUsersManager();


	@BeforeClass
	public void startBrowserAndDriver() throws MalformedURLException
	{
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("SearchTestInternal started at: " + dateFormat.format(date)); //2016/11/16 12:08:43
		
	//Choose Browser & Driver
		//System.setProperty("webdriver.chrome.driver", "C:/drivers/chromedriver.exe");	//Chrome
		//driver=new ChromeDriver();													//Chrome
		//System.setProperty("webdriver.gecko.driver", "C:/drivers/geckodriver.exe");	//FireFox
		//driver = new FirefoxDriver();													//FireFox
		driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.firefox()); //Grid-FireFox
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		System.out.println("================Browser Started================");

	}
	
	
	

	@Test(threadPoolSize = 1, invocationCount = 3)
	public void startupWebDriver() throws InterruptedException 
	{
		LawyerportUser user = users.getUserAccount();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		driver.get(baseURL);
		driver.manage().window().maximize();
        driver.findElement(By.id("signin-button")).click();
        driver.findElement(By.id("username")).sendKeys(user.getUsername());
        driver.findElement(By.id("password")).sendKeys("Testpassword1!!");
        driver.findElement(By.id("clientCode")).sendKeys("Selenium");
        driver.findElement(By.id("submitButton")).click();

		 
	//Directory SearchTest01 (1/25)		
		System.out.println("Running People Search");
		driver.findElement(By.id("navDir")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("Bob");
		driver.findElement(By.id("form-field-4")).sendKeys("Clifford");
		driver.findElement(By.id("dir-run-advanced-search")).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("leftContent1")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-header-title")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-wrapper")));
		Thread.sleep(3000);

	//Directory ViewTest01 (2-6/25)			
		System.out.println("Clicking Bob Clifford's Profile Link");
		driver.findElement(By.className("search-click-through")).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("profile-pic")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("summary-profile-container")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("summary-profile-info")));
		Thread.sleep(3000);
		driver.findElement(By.id("pA-toc-practice-areas")).click();			
		Thread.sleep(3000);
		driver.findElement(By.id("pA-toc-affiliations")).click();			
		Thread.sleep(3000);
		driver.findElement(By.id("pA-toc-news-li")).click();			
		Thread.sleep(3000);
		driver.findElement(By.id("pA-toc-admissions")).click();			
		Thread.sleep(3000);		

	//Directory SearchTest02 (7/25)		
		System.out.println("Running People Search");
		driver.findElement(By.id("navDir")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("Peter");
		driver.findElement(By.id("form-field-4")).sendKeys("Mierzwa");
		driver.findElement(By.id("dir-run-advanced-search")).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("leftContent1")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-header-title")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-wrapper")));
		Thread.sleep(3000);

	//Directory ViewTest02 (8-13/25)			
		System.out.println("Clicking Peter Mierzwa's Profile Link");
		driver.findElement(By.className("search-click-through")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("pA-toc-practice-areas")).click();			
		Thread.sleep(3000);
		driver.findElement(By.id("pA-toc-affiliations")).click();			
		Thread.sleep(3000);
		driver.findElement(By.id("pA-toc-news")).click();			
		Thread.sleep(3000);
		driver.findElement(By.id("pA-toc-admissions")).click();			
		Thread.sleep(3000);	
		driver.findElement(By.id("pA-toc-cases")).click();			
		Thread.sleep(3000);	

	//Directory SearchTest03 (14/25)		
		System.out.println("Running People Search");
		driver.findElement(By.id("navDir")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("subtab-org")).click();
		driver.findElement(By.id("form-field-0")).sendKeys("IL Supreme Court");
		driver.findElement(By.id("dir-org-run-advanced-search")).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("leftContent1")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-header-title")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-wrapper")));
		Thread.sleep(3000);

	//Directory ViewTest03 (15-17/25)			
		System.out.println("Clicking Supreme Court's Profile Link");
		driver.findElement(By.className("result-description")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("org-toc-crt-org")).click();			
		Thread.sleep(3000);
		driver.findElement(By.id("org-toc-crt-our-people")).click();			
		Thread.sleep(3000);

		
		
	//Directory SearchTest04 (18/25)		
		System.out.println("Running People Search");
		driver.findElement(By.id("navDir")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("clare");
		driver.findElement(By.id("form-field-4")).sendKeys("mcwilliams");
		driver.findElement(By.id("dir-run-advanced-search")).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("leftContent1")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-header-title")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-wrapper")));
		Thread.sleep(3000);		
		
	//Directory ViewTest04 (19/25)			
		System.out.println("Clicking Bob McWilliams Profile Link");
		driver.findElement(By.className("search-click-through")).click();
		Thread.sleep(3000);
		

	//News SearchTest01 (20/25)	
		System.out.println("Running News Search");
		driver.findElement(By.id("navNews")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-4")).sendKeys("Law Bulletin Media");
		driver.findElement(By.id("news-run-advanced-search")).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("left-container")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("news-center-container")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("search-detail-container")));
		Thread.sleep(3000);

	//News ViewTest01 (21/25)
		System.out.println("Clicking first search result");
		driver.findElement(By.className("search-click-through")).click();
		Thread.sleep(3000);
				
	//News ViewTest02 (22/25)
		System.out.println("Navigating Directly to News Article");
		driver.get(baseURL + "/articles#/article/LB/100052227/1003603/P/undefined");
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("article-description")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("article-header-row")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("news-center-container")));	
		Thread.sleep(3000);		
		
	//JVR SearchTest01 (23/25)		
		System.out.println("Running Verdict & Settlement Search");
		driver.findElement(By.id("navRes")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("verdicts-settlements")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-field-0")).sendKeys("bus AND car");
/*		driver.findElement(By.id("add-jv-terms")).click();	
		Thread.sleep(3000);
		driver.findElement(By.id("jv-term-select4720")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("jv-term-select4765")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("jv-term-select-select-all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-button-Save")).click();
*/		Thread.sleep(3000);		
		driver.findElement(By.id("jv-run-advanced-search")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("form-button-Submit")).click();
		Thread.sleep(3000);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("verdict-result-header")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("paginator-top-result-node")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("result-node")));
		Thread.sleep(3000);

	//JVR ViewTest02 (24/25)		
		System.out.println("JVR Clicking All + View Selected");
		driver.findElement(By.id("all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("view-selected")).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);		
				
		
		
	//JVR ViewTest02 (25/25)
		System.out.println("Navigating Directly to JVR Case");
		driver.get(baseURL + "/research#/verdicts-settlements/view-results/&fromNews=yes&juryVerdictId=1016402/1016402");	
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("case-article-pubName-0")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("print-button-anchor")));
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("case-article-article-0")));
		Thread.sleep(3000);			
		
	}
		
		
		@AfterClass
		public void shutdown()
		{
			driver.manage().deleteAllCookies();
			driver.quit();
			
			
		}
		
		
		
	}